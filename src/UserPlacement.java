

import it.unipi.ing.path_finder.*;

import java.io.IOException;
import java.io.ObjectInputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class UserPlacement
 */
@WebServlet("/UserPlacement")
public class UserPlacement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPlacement() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ObjectInputStream in = new ObjectInputStream(req.getInputStream());
		BeaconList bl = null;
        try {
        	bl = (BeaconList) in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        XMLPlacements.addBeaconsRate(bl.getUserID(), bl.getDay(), bl.getBeacons(), this.getServletContext().getRealPath(""));
        
	}
}
