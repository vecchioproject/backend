

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PathListServlet
 */
@WebServlet("/PathListServlet")
public class PathListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PathListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        response.setContentType("text/plain");
	        response.getWriter().println(this.getServletContext().getRealPath(""));
	    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        ObjectInputStream in = new ObjectInputStream(req.getInputStream());
        ArrayList<String> al=null;
        try {
            al = (ArrayList<String>) in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(al.size() > 1)
        	for(int i=0; i<al.size()-1; i++)
        		if(!al.get(i).equals(al.get(i+1)))
        			XMLGraph.addEdgeToGraph(al.get(i), al.get(i+1), this.getServletContext().getRealPath(""));
        	
    }
}
