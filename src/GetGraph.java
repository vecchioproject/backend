

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javafx.util.Pair;

/**
 * Servlet implementation class GetGraph
 */
@WebServlet("/GetGraph")
public class GetGraph extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String graphImage = "Graph.png";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetGraph() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {  
		
		String path = this.getServletContext().getRealPath("");
		File directory = new File(path);
		ObjectOutputStream out = new ObjectOutputStream(resp.getOutputStream());
		ArrayList<Pair<String, String>> al = XMLPlacements.getLocation(this.getServletContext().getRealPath(""));
		if(XMLGraph.generateGraphImage(al, path)){
			File f = new File(directory, graphImage);
			byte[] content = Files.readAllBytes(f.toPath());
			out.writeObject(content);
		}
		else
			out.writeObject(null);
	}	

}
