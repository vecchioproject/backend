import it.unipi.ing.path_finder.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.util.Pair;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLPlacements {
	
	private static String nameDirectory = "UsersPlacement/";
	private static final int dayBefore = 45;
	
	private static Document readUSerPlacement(File f) throws ParserConfigurationException, FileNotFoundException, SAXException, IOException{
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new FileInputStream(f));
		return doc;
	}
	
	private static void writeUserPlacement(Document doc, File f) throws TransformerFactoryConfigurationError, TransformerException{
		
		PrintWriter writer = null;
		Transformer tf = null;
		tf = TransformerFactory.newInstance().newTransformer();
    	tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    	tf.setOutputProperty(OutputKeys.INDENT, "yes");
    	StringWriter out = new StringWriter();
    	tf.transform(new DOMSource(doc), new StreamResult(out));
    	try {
        	writer = new PrintWriter(f);
        	String fileText = out.toString();
        	fileText = fileText.replaceAll("(?m)^[ \t]*\r?\n", "");
        	writer.write(fileText);
    	} catch (FileNotFoundException e) {
        	e.printStackTrace();
    	}
    	finally{
        	writer.close();
    	}
	}
	
	private static Document removeExpiredDates(Document doc, String dateCheck){
		
		NodeList dayViews = doc.getElementsByTagName("DayView");
		
        for(int i=dayViews.getLength()-1; i>=0; i--){
        	Element d = (Element)dayViews.item(i);
        	if (d.getAttribute("date").compareTo(dateCheck) < 0){
        		d.getParentNode().removeChild(d);
        	}
        	else{
        		break;
        	}
        }
		return doc;
	}
	
	private static String getPreviousDay(int daysBefore){
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -daysBefore);
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(cal.getTime());
	}
	
	private static void initFile(File f){
		
		String emptyFile = "<?xml version='1.0' encoding='UTF-8'?><Placement xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='UserPlacement.xsd'></Placement> ";
		PrintWriter writer = null;
		try {
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
            writer.write(emptyFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
                writer.close();
        }
	}
	
	public static void addBeaconsRate(String fileName, String date, ArrayList<BeaconElem> beacons, String path){
		
		File directory = new File(path + nameDirectory);
		
		//If the directory doesn't exist it'll be created
		if (!directory.exists()){
			directory.mkdir();
		}

		File f = new File(directory, fileName + ".xml");
		FileChannel channel = null;
		FileLock lock = null;
		
		try {
			channel = new RandomAccessFile(f, "rw").getChannel();
			lock = channel.lock();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//If the file doesn't exist it'll be created and initialized
			if (f.length() == 0)
				initFile(f);
		//Load the file in memory
		Document doc = null;
        try {
            doc = readUSerPlacement(f);         
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        
        //Adding the DayViews into DOM
        NodeList elems = doc.getElementsByTagName("DayView");
        for(int i=0; i<beacons.size(); i++){
			BeaconElem b = beacons.get(i);
			boolean existsNode = false;
			Element dayView = null;
			int j;
			for(j=0; j<elems.getLength() && !existsNode; j++){
				dayView = (Element) elems.item(j);
				if (dayView.getAttribute("date").compareTo(date) < 0){
					break;
				}
				else{
					if (dayView.getAttribute("beacon").equals(b.getBeaconID()) && dayView.getAttribute("date").equals(date)){
						dayView.setTextContent("" + (Integer.parseInt(dayView.getTextContent()) + b.getBeaconCount()));
						existsNode = true;
					}
				}
			}
			
			if (!existsNode){
				Element newDayView = doc.createElement("DayView");
				newDayView.setAttribute("beacon", b.getBeaconID());
				newDayView.setAttribute("date", date);
				newDayView.setTextContent("" + b.getBeaconCount());
				Element parent = (Element) doc.getElementsByTagName("Placement").item(0);
				if(dayView == null)
					parent.appendChild(newDayView);
				else{
					if(j == elems.getLength()){
						dayView.getParentNode().appendChild(newDayView);
					}
					else
						parent.insertBefore(newDayView, dayView);
				}
			}
        }
        
        //remove the Expired DayViews
        String dateCheck = getPreviousDay(dayBefore);
        doc = removeExpiredDates(doc, dateCheck);
        
        //Save the file in the storage
        try {
        	writeUserPlacement(doc, f);
        } catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
    		e.printStackTrace();
        } catch (TransformerException e) {
        	e.printStackTrace();
        }
        
        try {
			lock.release();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				channel.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private static String findBestBeacon(ArrayList<Pair<String, Integer>> al){
		String beacon = null;
		int value = 0;
		
		for(Pair<String, Integer> p : al){
			if(p.getValue().intValue() > value){
				value = p.getValue().intValue();
				beacon = p.getKey();
			}
		}
		
		return beacon;
	}
	
	private static void insertInArray(ArrayList<Pair<String, Integer>> al, String beacon, Integer value){
		
		boolean found = false;
		int oldValue = 0;
		Pair<String, Integer> p;
		int i = 0;
		for(i=0; i<al.size(); i++){
			if(al.get(i).getKey().equals(beacon)){
				found = true;
				break;
			}
		}
		//If exists remove the oldVaue
		if(found){
			oldValue = al.get(i).getValue().intValue();
			al.remove(i);
		}
		Integer integer = new Integer(oldValue + value);
		p = new Pair<String, Integer>(beacon, integer);
		al.add(p);
	}
	
	public static ArrayList<Pair<String, String>> getLocation(String path){
		
		ArrayList<Pair<String, String>> list = new ArrayList<>();
		File directory = new File(path + nameDirectory);
		
		if(!directory.exists())
			return null;
		
		for(File f : directory.listFiles()){
			String[] split = f.getName().split("\\.");
			String user = split[0];
			FileChannel channel = null;
			FileLock lock = null;
			
			//Obtain the lock on the file
			try {
				channel = new RandomAccessFile(f, "rw").getChannel();
				lock = channel.lock();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Load the file in memory
			Document doc = null;
	        try {
	            doc = readUSerPlacement(f);         
	        } catch (ParserConfigurationException e) {
	            e.printStackTrace();
	        } catch (SAXException | IOException e) {
	            e.printStackTrace();
	        }
			//Release the lock
			try {
				lock.release();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally{
				try {
					channel.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//Ricerca dei 
			ArrayList<Pair<String, Integer>> beaconList = new ArrayList<>();
			NodeList dayViews = doc.getElementsByTagName("DayView");
			for(int i=0; i<dayViews.getLength(); i++){
				Element el = (Element) dayViews.item(i);
				String beacon = el.getAttribute("beacon");
				int value = Integer.parseInt(el.getTextContent());
				insertInArray(beaconList, beacon, value);
			}
			String bestBeacon = findBestBeacon(beaconList);
			list.add(new Pair<String, String>(bestBeacon, user));
		}
		return list;
	}

}