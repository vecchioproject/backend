import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javafx.util.Pair;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

public class XMLGraph {

    private static String xmgFile = "Graph.graphml";
    private static String graphImage = "Graph.png";

    public XMLGraph(){

    }
    
    public static synchronized void addEdgeToGraph(String n1, String n2, String path){

        String emptyFile ="<?xml version='1.0' encoding='UTF-8'?><graphml xmlns='http://graphml.graphdrawing.org/xmlns' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd'><graph id='G' edgedefault='undirected'></graph></graphml>";
        File directory = new File(path);
        File f = new File(directory, xmgFile);
        PrintWriter writer = null;
        if (!f.exists()){
            try {
            	f.createNewFile();
                writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                writer.write(emptyFile);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally{
                    writer.close();
            }
        }
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);

        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Document doc = null;
        try {
            doc = db.parse(new FileInputStream(f));
        } catch (SAXException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String minor, major;
        if (n1.compareTo(n2) < 0){
            minor = n1;
            major = n2;
        }
        else {
            minor = n2;
            major = n1;
        }

        NodeList elems = doc.getElementsByTagName("edge");

        for(int i=0; i<elems.getLength(); i++){
            Element e = (Element) elems.item(i);
            if(e.getAttribute("id").equals(minor + "-" + major))
                return;
        }

        Node parent = doc.getElementsByTagName("graph").item(0);

        boolean foundMin = false, foundMaj = false;
        elems = doc.getElementsByTagName("node");
        for (int i=0; i<elems.getLength() && !(foundMin && foundMaj); i++){
            String id = ((Element)elems.item(i)).getAttribute("id");
            if(id.equals(minor))
                foundMin = true;
            if(id.equals(major))
                foundMaj = true;
        }
        if (!foundMin){
            Element node = doc.createElement("node");
            node.setAttribute("id", minor);
            parent.insertBefore(node, parent.getFirstChild());
        }
        if (!foundMaj){
            Element node = doc.createElement("node");
            node.setAttribute("id", major);
            parent.insertBefore(node, parent.getFirstChild());
        }

        Element node = doc.createElement("edge");
        node.setAttribute("id", minor + "-" + major);
        node.setAttribute("source", minor);
        node.setAttribute("target", major);
        parent.appendChild(node);

        Transformer tf = null;
        try {
            tf = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException
                | TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter out = new StringWriter();
        try {
            tf.transform(new DOMSource(doc), new StreamResult(out));
        } catch (TransformerException e) {
            e.printStackTrace();
            }
    
            try {
                writer = new PrintWriter(f);
                writer.write(out.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            finally{
                    writer.close();
            }
        }
    
    public static synchronized boolean generateGraphImage(ArrayList<Pair<String, String>> al, String path){
    	
    	File directory = new File(path);
        File graphmlFile = new File(directory, xmgFile);
        
        if(!graphmlFile.exists()){
        	return false;
        }
    	
		//Init a project - and therefore a workspace
		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		Workspace workspace = pc.getCurrentWorkspace();

		// get import controller
		ImportController importController = Lookup.getDefault().lookup(ImportController.class);

		//Import file
		Container container = null;
		try {
			container = importController.importFile(graphmlFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		//Append imported data to GraphAPI
		importController.process(container, new DefaultProcessor(), workspace);
		
		GraphModel GM = Lookup.getDefault().lookup(GraphController.class).getModel();
		for(Pair<String, String> a : al){
			org.gephi.graph.api.Node node = GM.getGraph().getNode(a.getKey());
			node.getNodeData().setLabel(node.getNodeData().getLabel() +": "+a.getValue());
		}
		
		PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
		PreviewModel model = previewController.getModel();
		model.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
		model.getProperties().putValue(PreviewProperty.CATEGORY_NODE_LABELS, Boolean.TRUE);
		model.getProperties().putValue(PreviewProperty.NODE_LABEL_PROPORTIONAL_SIZE, Boolean.TRUE);
		model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(10));
		model.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
		model.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(new Color(30,115,168)));
		model.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, new Color(233,233,233));
		
		//Export graph to PDF
		ExportController ec = Lookup.getDefault().lookup(ExportController.class);
		try {
			ec.exportFile(new File(directory, graphImage));
			//ec.exportStream(null, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return true;
    }
}
