package it.unipi.ing.path_finder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class BeaconList implements Serializable{

    private static final long serialVersionUID = 1L;

    private String userID;
    private ArrayList<BeaconElem> beacons;
    private String day;

    public BeaconList()
    {
        beacons = new ArrayList<BeaconElem>();
    }

    public void incrementBeaconCount(String beaconID)
    {
        BeaconElem beaconElem = findInList(beaconID);
        if(beaconElem==null)
        {
            BeaconElem newBeaconElem = new BeaconElem();
            newBeaconElem.beaconID = beaconID;
            newBeaconElem.beaconCount = 1;
            beacons.add(newBeaconElem);
        }
        else
        {
            beaconElem.beaconCount++;
        }
    }

    /* If element is not already in the  */
    public void addBeaconElem(String ID, int count){
        BeaconElem beaconElem = findInList(ID);
        if(beaconElem==null)
        {
            BeaconElem newBeaconElem = new BeaconElem();
            newBeaconElem.beaconID = ID;
            newBeaconElem.beaconCount = count;
            beacons.add(newBeaconElem);
        }
        else
        {
            beaconElem.beaconCount = count;
        }
    }

    public BeaconElem findInList(String ID){
        for(BeaconElem b : beacons ) {
            if(b.beaconID.equals(ID)) {
                return b;
            }
        }
        return null;
    }

    /* Gets a file and loads its content inside the class*/
    public void parseFile(File file) throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line;
        try {
            while ((line = br.readLine()) != null && line.length() != 0) {
                String[] split = line.split("\\s");
                //Log.d("LINE", " "+line);
                addBeaconElem(split[0], Integer.parseInt(split[1]));
                //Log.d("BEACONLIST LOADED", "value " + ": " + split[0] + " - " + split[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
    }

    public String dumpListToString()
    {
        String text = "";
        for(BeaconElem b : beacons ){
            text = text + b.beaconID + " " + b.beaconCount + "\n";
        }
        //Log.d("LIST TO BE SAVED:", text);
        return text;
    }

    public ArrayList<BeaconElem> getBeacons() {
        return beacons;
    }
    public void setBeacons(ArrayList<BeaconElem> beacons) {
        this.beacons = beacons;
    }

    public String getDay() {
        return day;
    }
    public void setDay(String day){
        this.day = day;
    }

    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID){
        this.userID = userID;
    }

}
