package it.unipi.ing.path_finder;

import java.io.Serializable;

public class BeaconElem implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String beaconID;
    protected int beaconCount;

    public String getBeaconID() {
        return beaconID;
    }
    public void setBeaconID(String beaconID) {
        this.beaconID = beaconID;
    }
    public int getBeaconCount() {
        return beaconCount;
    }
    public void setBeaconCount(int beaconCount) {
        this.beaconCount = beaconCount;
    }

}